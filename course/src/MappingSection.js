import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MappingSectionHeading from './MappingSectionHeading';
import MappingSectionTable from './MappingSectionTable';
import MappingSectionSummary from './MappingSectionSummary';

const styles = theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '70%',
    flexShrink: 0,
  },
});

class MappingSection extends React.Component {
  render() {
    const panel = this.props.expanded;
    const { nltkStopWords, panels, nltkWordCount, selectedPanel, coverage, updateCoverage } = this.props;

    return (
      <div>
        <MappingSectionTable panel={panel} panels={panels} nltkStopWords={nltkStopWords} nltkWordCount={nltkWordCount} selectedPanel={selectedPanel} coverage={coverage} updateCoverage={updateCoverage}></MappingSectionTable>
        <MappingSectionSummary panel={panel}></MappingSectionSummary>
      </div>
    )
  }
}

MappingSection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MappingSection);
