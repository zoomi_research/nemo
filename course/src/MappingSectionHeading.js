import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
  mapping: {
    marginTop: '20px',
    padding: '20px',
    textAlign: 'center'
  },
  heading: {
    fontSize: 16,

  },
    root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit,
  },
});

class MappingSectionHeading extends React.Component {
  state = {
    comparison: 'Module Content Against Regulations',
  };

  handleChangeHeading = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    const { classes, panel } = this.props;
    const mod2 = panel ? Number(panel.slice(-1)) - 1 : -1;
    let regulation;
    if (mod2 > 5) {
      regulation = <Typography><span><b>Module {mod2}</b> is meant to cover <b>Regulation 2</b></span></Typography>
    } else if (mod2 > 0) {
      regulation = <Typography><span><b>Module {mod2}</b> is meant to cover <b>Regulations 1 and 2</b></span></Typography>
    } else {
      regulation = <span></span>
    }

    return (
      <div>
        {mod2 < 1 &&
          <Paper className={classes.mapping}>
            <p><em>Select a module to display its coverage and recommendations</em></p>
          </Paper>
        }
        {mod2 > 0 &&
          <div className={classes.mapping}>
            <FormControl className={classes.formControl}>
              <Typography className={classes.heading}>Comparison of</Typography>
              <Select
                value={this.state.comparison}
                onChange={this.handleChangeHeading}
                name="comparison"
                displayEmpty
                className={classes.selectEmpty}
              >
                <MenuItem value='Module Content Against Regulations'>Module Content Against Regulations</MenuItem>
                <MenuItem value='Module Content Against Asesssments'>Module Content Against Assessment</MenuItem>
                <MenuItem value='Module Assessment Against Regulations'>Module Assessment Against Regulations</MenuItem>
              </Select>
            </FormControl>
            {regulation}
          </div>
        }
      </div>
    )
  }
}

MappingSectionHeading.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MappingSectionHeading);
