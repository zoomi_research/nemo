import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  title: {
    fontSize: theme.typography.pxToRem(16),
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10,
  },
  objective: {
    paddingLeft: 20,
  },
  objectives: {
    padding: 5,
  },
  container: {
    padding: 15,
  },
  selected: {
    background: '#ADD8E6'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '70%',
    flexShrink: 0,
  },
  root: {
  }
});


class ObjectiveSection extends React.Component {
  state = {
    heading: null,
    sections: [],
  };

  async componentWillMount() {
    if (this.state.heading === null) {
      try {
        const response = await fetch('api/objectives');
        const json = await response.json();
        this.setState( {
          heading: json.heading,
          sections: json.sections
        });
      } catch (error) {
        console.log('Error fetching Objectives');
      }
    }
  };

  setClass(id, reg1Active, reg2Active) {
    const { classes } = this.props;
    if (id === 1 && reg1Active) {
      return classes.selected;
    }
    if (id === 2 && reg2Active) {
      return classes.selected;
    }
  }

  render() {
    const { heading, sections } = this.state;
    const { classes } = this.props;
    const panel = this.props.expanded
    const mod = panel ? panel.slice(-1) : 0;
    let reg1Active = false;
    let reg2Active = false;
    if (mod > 6) {
      reg2Active = true;
    } else if (mod > 1) {
      reg1Active = true;
      reg2Active = true;
    }

    return (
      <div className={classes.objective}>
        <Typography className={classes.title}>{heading}</Typography>
        <Paper className={classes.objectives}>
          {sections.map((section) =>
            <div key={section.id} className={
              this.setClass(section.id, reg1Active, reg2Active)
            }>
              <div className={classes.container}>
                <Typography><b>{section.name}</b></Typography>
                <Typography>{section.document}</Typography>
                <ul>
                  {section.reference.map((reference, index) =>
                    <li key={index}><Typography>{reference}</Typography></li>
                  )}
                </ul>
              </div>
            </div>
          )}
        </Paper>
      </div>
    )
  }
}

ObjectiveSection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ObjectiveSection);
