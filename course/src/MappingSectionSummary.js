import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  summary: {
    marginTop: '20px',
    marginBottom: '50px',
    float: 'right',
    paddingLeft: '20px'
  },
  pos: {
    color: 'green'
  },
  neg: {
    color: 'red'
  },
  first: {
    padding: '20px'
  }
});

class MappingSectionSummary extends React.Component {
  render() {
    const { classes, panel } = this.props;
    const mod = panel ? Number(panel.slice(-1)) - 1 : 0;
    const health = mod === 1 ? 'healthy' : 'not healthy'
    const healthClass = health === 'healthy' ? 'pos' : 'neg';
    let first;
    if (mod !== 0) {
      first = <div className={classes.first}><b>Module {mod} </b> coverage of its objectives is <b className={healthClass === 'pos' ? classes.pos : classes.neg}>{health}</b>. Make the changes above to make it even better.</div>
    }
    return (
      <Paper className={classes.summary}>
          {first}
      </Paper>
    )
  }
}

MappingSectionSummary.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MappingSectionSummary);
