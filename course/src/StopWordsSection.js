import React from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    marginTop: 20,
  },
  stopWordsHeader: {
    color: 'purple',
    fontSize: 16,
  },
  synTag: {
    display: 'inline-block',
    paddingLeft: 3
  },
  customStopWords: {
    marginTop: 0,
    marginBottom: 0,
  },
  stopwordContainer: {
    paddingLeft: 5,
    margin: 0,
  },
  stopWords: {
    marginTop: 0,
    marginBottom: 0,
    paddingLeft: 0,
    columnCount: 4,
    width: '65%'
  },
  stopWord: {
    listStyleType: 'none',
    fontSize: 12
  },
  stopWordsCount: {
    fontSize: 12,
    marginLeft: 10,
    padding: '2px 4px 2px 4px',
    color: 'white',
    backgroundColor: 'purple',
    borderRadius: '20%'
  },
  restorable: {
    color: '#3f51b5'
  }
});

class StopWordsSection extends React.Component {
  state = {
    expanded: null,
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
    });
  };

  clickedRestoreTopic = (id, event) => {
    event.preventDefault();
    const topic = event.currentTarget.innerHTML;
    this.props.restoreTopic(id, topic);
  };

  clickedRestoreSynonym = (syn, event) => {
    event.preventDefault();
    this.props.restoreSynonym(syn);
  };

  render() {
    const {
      classes,
      nltkStopWords,
      panel,
      stopSynonymRows,
      stopWordRows,
      stopWordsCount
    } = this.props;

    const mod = panel ? panel.slice(-1) : 0;

    return (
      <div className={classes.root}>
        {mod > 1 &&
          <Paper>
            <ExpansionPanel key='stopWords' defaultExpanded={false} onChange={this.handleChange}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading}>
                  <span className={classes.stopWordsHeader}>Custom Stop Words</span>
                  {stopWordRows.length > 0 &&
                    <span className={classes.stopWordsCount}>{stopWordRows.length}</span>
                  }
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                {stopWordRows.length > 0 &&
                  <div className={classes.customStopWords}>
                    <ul className={classes.stopwordContainer}>
                      <li className={classes.stopWord}><Typography>Click to remove:</Typography></li>
                      <div>
                        {stopWordRows.map((row) =>
                          <li className={classes.stopWord} key={row.id}>
                            <Typography>
                              <a
                                href='#'
                                className={classes.restorable}
                                onClick={this.clickedRestoreTopic.bind(this, row.id)}
                              >
                                {row.topic}
                              </a>
                            </Typography>
                          </li>
                        )}
                        {stopSynonymRows.map((row) =>
                          row.synonyms.map((syn) =>
                            <li className={classes.stopWord} key={syn}>
                              <Typography>
                                <a
                                  href='#'
                                  className={classes.restorable}
                                  onClick={this.clickedRestoreSynonym.bind(this, syn)}
                                >
                                  {syn}<span className={classes.synTag}><em>(synonym)</em></span>
                                </a>
                              </Typography>
                            </li>
                          )
                        )}
                      </div>
                    </ul>
                  </div>
                }
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Paper>
        }
      </div>
    );
  }
}

StopWordsSection.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(StopWordsSection);
