import React from 'react';
import PropTypes from 'prop-types';
import StopWordsSection from './StopWordsSection';
import MappingSectionMeter from './MappingSectionMeter';

import { withStyles } from '@material-ui/core/styles';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

//TABLE
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

//FORM
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

//SPINNER
import CircularProgress from '@material-ui/core/CircularProgress';


const theme = createMuiTheme({
  overrides: {
    MuiTableCell: {
      root: {
        "&:last-child": {
          paddingRight: 0,
          minWidth: 150
        },
        "&:first-child": {
          minWidth: 140
        }
      }
    }
  }
});

// TODO: use vars instead of values here
const styles = theme => ({
  root: {
    overflowX: 'auto',
    width: '100%',
  },
  rowStyle: {
    paddingTop: 0,
    paddingBottom: 0,
  },
  synList: {
    color: 'gray'
  },
  red: {
    color: 'red'
  },
  yellow: {
    color: 'orange'
  },
  green: {
    color: 'green'
  },
  invisible: {
    color: 'white',
    fontSize: 1
  },
  toggleButton: {
    paddingTop: 10
  },
  customize: {
    float: 'right',
  },
  toggleSynonym: {
    color: '#3f51b5',
    fontSize: 10,
  },
  synonymInput: {
    color: '#3f51b5',
    fontSize: 14,
    width: 110,
  },
  synonymAdd: {
    marginTop: 0,
    marginLeft: 0,
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 10,
    color: '#3f51b5',
  },
  addedSynonym: {
    color: 'grey',
    marginTop: 2,
    paddingLeft: 10,
  },
  addSynColumn: {
    padding: 0,
  },
  icon: {
    color: 'purple',
    fontSize: 16,
  },
  stopWordColumn: {
    color: 'purple',
    fontSize: 10,
    maxWidth: 20,
    padding: 0,
  },
  helpText: {
    color: '#3f51b5',
    fontSize: 11,
    paddingLeft: 5,
  },
  synonymPhrase: {
    color: '#3f51b5',
  },
  lightTooltip: {
    background: theme.palette.common.white,
    color: 'purple',
    boxShadow: theme.shadows[1],
    fontSize: 14,
  },
  labelTooltip: {
    background: theme.palette.common.white,
    color: 'purple',
    boxShadow: theme.shadows[1],
    fontSize: 14,
    float: 'left',
    width: 140,
  },
  info: {
    float: 'right',
    marginTop: 10,
    paddingLeft: 5,
    paddingRight: 5,
    color: 'white',
    background: 'slateGray',
    borderRadius: '50%',
    fontSize: 12,
    fontWeight: 500
  },
  info2: {
    marginTop: 10,
    marginLeft: 10,
    paddingLeft: 5,
    paddingRight: 5,
    color: 'white',
    background: 'slateGray',
    borderRadius: '50%',
    fontSize: 10,
    fontWeight: 400
  },
  // HEADING
  summary: {
    padding: '20px',
    marginTop: '20px',
    textAlign: 'center'
  },
  mapping: {
    marginTop: '20px',
    textAlign: 'center'
  },
  heading: {
    fontSize: 16,

  },
    root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit,
  },
  // Spinner
  progress: {
    marginLeft: 330,
    marginTop: 180,
    marginBottom: 200,
  },
  centerPanel: {
    clear: 'both'
  }
});


class MappingSectionTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fullTable: false, // Temp
      synonymsTable: false, // Temp
      synonymsDisplayed: false, // Temp
      stopButtonDisplayed: false, // Temp
      stopSynonymRows: [],
      stopWordRows: [],
      stopWordsCount: this.props.nltkWordCount,
      fetchCompleted: false,
      comparison: 'Module Content Against Regulations',
      rows: [],
      refreshTable: true,
      loading: false,
      addedSynonyms: [],
      removedSynonyms: [],
    };
  }

  toggleTable = () => {
    const {fullTable} = this.state;
    this.setState({ fullTable: !fullTable });
  };

  toggleTopics = () => {
    const {synonymsTable} = this.state;
    this.setState({ synonymsTable: !synonymsTable });
  };

  toggleSynonyms = () => {
    const {synonymsDisplayed} = this.state;
    this.setState({ synonymsDisplayed: !synonymsDisplayed });
  };

  toggleStopButton = () => {
    const {stopButtonDisplayed} = this.state;
    this.setState({ stopButtonDisplayed: !stopButtonDisplayed });
  };


  handleChange = (data, event) => {
    if (event.target.value !== '') {
      this.setState({
        userSynonym: event.target.value,
        targetRow: data,
      });
    }
  }

  addStopWord = (data, event) => {
    event.preventDefault();
    const { stopWordRows, stopSynonymRows } = this.state;
    const { nltkWordCount } =  this.props;

    // Add stopword flag to removed topics
    let newState = Object.assign({}, this.state);
    const st = newState.rows.find(row => row.id === data);
    st.stopWord = true;
    const topicSynonyms = { id: st.id, topic: st.topic, synonyms: st.synonyms };
    // Delete synonyms from topics (a copy of originalSynonyms is intact)
    st.synonyms = [];
    this.setState(st);

    const newStopWords = stopWordRows.concat(st);
    const newStopSynonyms = stopSynonymRows;
    newStopSynonyms.push(topicSynonyms);
    const count = nltkWordCount + newStopWords.length + newStopSynonyms.length;
    this.setState({
      stopWordRows: newStopWords,
      stopSynonymRows: newStopSynonyms,
      stopWordsCount: count
    });
  }

  addSynonym = (topic, event) => {
    // TODO: store as array not string
    event.preventDefault();
    const { userSynonym, targetRow, rows } = this.state;
    if (userSynonym) {
      let newSynonyms;
      for (let i =  0; i < rows.length; i++) {
        if (rows[i].id === targetRow) {
          if (rows[i].synonyms) {
            newSynonyms = rows[i].synonyms;
            newSynonyms.push(userSynonym);
          } else {
            newSynonyms = [userSynonym];
          }
          break;
        }
      }
      const synonymPair = [topic, userSynonym]
      const st = this.state.addedSynonyms.concat(synonymPair);
      console.log('ADDING SYNONYM PAIR', st);

      this.setState({
        userSynonym: null,
        addedSynonym: st,
      }, () => console.log('LIST of synonym pairs', this.state.addedSynonyms));
    }
  };

  removeSynonym = (data, syn, event) => {
    event.preventDefault();
    let newState = Object.assign({}, this.state);
    const syns = newState.rows.find(row => row.id === data).synonyms
    const filteredSyn = syns.filter(s => s !== syn);
    newState.rows.find(row => row.id === data).synonyms = filteredSyn;
    this.setState(newState);
  };

  restoreTopic(id, topic) {
    const { stopWordRows, stopWordsCount } = this.state;
    // Remove stopword flag from restored topics
    // Synonyms are not restored at this step
    let newState = Object.assign({}, this.state);
    const st = newState.rows.find(row => row.id === id);

    st.stopWord = false;
    this.setState(st);

    const newStopWords = stopWordRows.filter(st => st.topic !== topic);
    const count = stopWordsCount - 1;
    this.setState({
      stopWordRows: newStopWords,
      stopWordsCount: count
    });
  };

  restoreSynonym(synonym) {
    console.log('CALLED restore synonym for', synonym)
    const { nltkWordCount } =  this.props;
    const { stopWordRows, stopSynonymRows } = this.state;
    // Remove synonyms from stopSynonymsList
    // TODO: use topic key to add back to the original topic
    const newStopSynonyms = stopSynonymRows.filter(st => !st.synonyms.includes(synonym));
    const count = nltkWordCount + stopWordRows.length + newStopSynonyms.length;
    this.setState({
      stopSynonymRows: newStopSynonyms,
      stopWordsCount: count
    });
  };

  async getNewRecommendations(panel) {
    const { synonymsTable, stopSynonymRows, stopWordRows, comparison, rows } = this.state;
    const { updateCoverage } = this.props

    const params = {
      panel: panel,
      addStopWords: stopWordRows,
      addStopSynonyms: stopSynonymRows,
      addSynonyms: [],
      removeSynonyms: [],
      comparison: comparison
    };

    try {
      console.log('GETTING New Recommendations');
      updateCoverage();
      const response = await fetch('api/mapping',
        { method: 'post',
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(params),
        }
      );
      const json = await response.json();
      console.log('Previous rows: ', this.state.rows);
      if (json) {
        console.log('GOT New Recommendations');
        console.log('UPDATING rows for ', panel);
        const rows = json.mapping;
        console.log('New rows', rows);
        console.log('Updating state and displaying table');
        this.setState({
          loading: false,
          rows: rows,
          synonymsTable: !synonymsTable,
        });
      }
    } catch (error) {
      console.log('Error fetching New Recommendations');
    }
  };

  updateTopics = (panel) => {
    // TODO: send user inputs and get new table data
    this.setState({ loading: true });
    this.getNewRecommendations(panel)
  };

  async componentDidUpdate(prevProps, prevState) {
    const { selectedPanel } = this.props;
    const { comparison } = this.state
    const refreshTable = (comparison !== prevState.comparison || selectedPanel !== prevProps.selectedPanel);
    if (refreshTable) {
      try {
        console.log('FETCHING mapping for', selectedPanel, comparison);
        const query = `api/mapping?panel=${selectedPanel}&comparison=${comparison}`
        const response = await fetch(query);
        const json = await response.json();
        this.setState( {
          rows: json.mapping,
        });
        console.log('NEW ROWS', this.state.rows);
      } catch (error) {
        console.log('Error fetching');
      }
    } else {
      console.log('DID NOT UPDATE ROWS');
    }
  };


  handleChangeHeading = event => {
    this.setState({
      [event.target.name]: event.target.value,
      refreshTable: true,
    });
  };

  render() {
    const {
      classes,
      panel,
      panels,
      nltkStopWords,
      customStopWords,
      selectedPanel,
      coverage
    } = this.props;
    const {
      synonymsDisplayed,
      stopButtonDisplayed,
      synonymsTable,
      fullTable,
      stopSynonymRows,
      stopWordRows,
      stopWordsCount,
      comparison,
      rows,
      refreshTable,
      loading,
    } = this.state;
    const mod = panel ? panel.slice(-1) : 0;
    const fullTableDisplayed = (mod !== 0 && fullTable === true);

    const synonymsButtonText = synonymsDisplayed === true ? "Clear Synonyms" : "Add Synonyms to Topic";
    const stopButtonText = stopButtonDisplayed === true ? "Done" : "Add Stop Word";

    let contentDisplayed;
    let regulationDisplayed;
    let assessmentDisplayed;
    let recommendationDisplayed;
    if (comparison === 'Module Content Against Assessment') {
      contentDisplayed = true;
      assessmentDisplayed = true;
      regulationDisplayed = false;
      recommendationDisplayed = false;
    } else if (comparison === 'Module Content Against Regulations') {
      contentDisplayed = true;
      assessmentDisplayed = false;
      regulationDisplayed = true;
      recommendationDisplayed = true;
    } else {
      contentDisplayed = false;
      assessmentDisplayed = true;
      regulationDisplayed = true;
      recommendationDisplayed = true;
    }

    // HEADING
    const mod2 = panel ? Number(panel.slice(-1)) - 1 : -1;
    let regulation;
    if (mod2 > 5) {
      regulation = <Typography><span><b>Module {mod2}</b> is meant to cover <b>Regulation 2</b></span></Typography>
    } else if (mod2 > 0) {
      regulation = <Typography><span><b>Module {mod2}</b> is meant to cover <b>Regulations 1 and 2</b></span></Typography>
    } else {
      regulation = <span></span>
    }

    return (
      <div>
        <div>
          {mod2 < 1 &&
            <div>
              <Paper className={classes.summary}>
                <h3>Overall Summary</h3>
                <p><em>Select a module on the left to display its coverage and recommendations</em></p>
                <Table className={classes.table} padding='dense'>
                  <TableHead>
                    <TableRow>
                      <TableCell>
                        Top Topics With Mismatch in Coverage 
                      </TableCell>
                      <TableCell align="left">
                        To Improve Coverage
                        <Tooltip
                          TransitionComponent={Fade}
                          TransitionProps={{ timeout: 500 }}
                          title="Green: Small/no gap Yellow: Moderate gap Red: Significant gap"
                          classes={{ tooltip: classes.labelTooltip }}
                        >
                          <span className={classes.info2}>i</span>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map(row => {
                      if (row.stopWord === false) {
                        return (
                          <TableRow key={row.id}>
                            <TableCell className={classes.rowStyle}>{row.topic.replace(/_/g, ' ')}</TableCell>
                            <TableCell className={
                              (() => {
                                if (recommendationDisplayed === true) {
                                  switch (row.toImprove) {
                                    case "Increase red":   return classes.red;
                                    case "Increase yellow":   return classes.yellow;
                                    case "Decrease red": return classes.red;
                                    case "Decrease yellow": return classes.yellow;
                                    default:  return classes.green;
                                  }
                                } else {
                                  switch (row.toImprove) {
                                    case "In spec":   return classes.green;
                                    default:  return classes.invisible;
                                  }
                                }
                              })()
                            }>
                              {row.toImprove.replace(' red', '').replace(' yellow', '')}
                            </TableCell>
                          </TableRow>
                        );
                      }
                    })}
                  </TableBody>
                </Table>
              </Paper>
            </div>
          }
          {mod2 > 0 &&
            <div className={classes.mapping}>
              <MappingSectionMeter panel={panel} panels={panels} comparison={comparison} coverage={coverage}></MappingSectionMeter>
              <FormControl className={classes.formControl}>
                <Typography className={classes.heading}>Comparison of</Typography>
                <Select
                  value={comparison}
                  onChange={this.handleChangeHeading}
                  name="comparison"
                  displayEmpty
                  className={classes.selectEmpty}
                >
                  <MenuItem value='Module Content Against Assessment'>Module Content Against Assessment</MenuItem>
                  <MenuItem value='Module Content Against Regulations'>Module Content Against Regulations</MenuItem>
                  <MenuItem value='Module Assessment Against Regulations'>Module Assessment Against Regulations</MenuItem>
                </Select>
              </FormControl>
              {regulation}
            </div>
          }
        </div>
        {mod > 1 &&
          <div className={classes.centerPanel}>
            {mod !== 0 && synonymsTable === false &&
              <div>
                <Tooltip
                  TransitionComponent={Fade}
                  TransitionProps={{ timeout: 500 }}
                  title="Edit the topic table to flag stopwords for removal or to add synonyms"
                  classes={{ tooltip: classes.lightTooltip }}
                >
                  <span className={classes.info}>i</span>
                </Tooltip>
                <Button color='primary' className={classes.customize} onClick={this.toggleTopics}>
                Customize Topics
                </Button>
                <Button color='primary' className={classes.toggleButton} onClick={this.toggleTable}>
                  {fullTable ? 'Less Details' : 'More Details'}
                </Button>
              </div>
            }
            {mod > 1 && synonymsTable === true &&
              <div>
                <Button color='primary' className={classes.toggleButton} onClick={this.toggleTopics}>
                  Back
                </Button>
                <Tooltip
                  TransitionComponent={Fade}
                  TransitionProps={{ timeout: 500 }}
                  title="This creates new recommendations based on user changes to stopwords and synonyms lists"
                  classes={{ tooltip: classes.lightTooltip }}
                >
                  <span className={classes.info}>i</span>
                </Tooltip>
                <Button color='primary' className={classes.customize} onClick={this.updateTopics.bind(this, panel)}>
                  Update Recommendations
                </Button>
              </div>
            }
          </div>
        }
        {loading === true &&
          <div className={classes.centerPanel}>
            <CircularProgress className={classes.progress} />
          </div>
        }
        {loading === false &&
          <Paper className={classes.root}>
            <MuiThemeProvider theme={theme}>
              {mod > 1 && synonymsTable === true &&
                < Table className={classes.table} padding='dense'>
                  <TableHead>
                    <TableRow>
                      <TableCell>
                        Key Topic
                      </TableCell>
                      <TableCell align='center'>
                        <Button size="small" className={classes.stopWordColumn} onClick={this.toggleStopButton}>
                          {stopButtonText}
                         </Button>
                      </TableCell>
                      <TableCell align="left">
                        <span>Synonyms</span>
                        <span className={classes.helpText}>- Click to remove</span>
                      </TableCell>
                      <TableCell align="center">
                         <Button size="small" className={classes.toggleSynonym} onClick={this.toggleSynonyms}>
                          {synonymsButtonText}
                        </Button>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map(row => {
                      if ( row.stopWord === false) {
                        return (
                          <TableRow key={row.id}>
                            <TableCell className={classes.rowStyle} component="th" scope="row">
                              {row.topic.replace(/_/g, ' ')}
                            </TableCell>
                            <TableCell align='center' className={classes.stopWordColumn}>
                              <form
                                style={{visibility: stopButtonDisplayed ? 'visible' : 'hidden'}}
                                onClick={this.addStopWord.bind(this, row.id)}
                              >
                                <IconButton>
                                  <DeleteOutlinedIcon className={classes.icon} />
                                </IconButton>
                              </form>
                            </TableCell>
                            <TableCell className={classes.rowStyle + classes.synList} align="left">
                              {row.synonyms.map((syn) => {
                                return (
                                  <div key={syn}>
                                    <a href='#' className={classes.synonymPhrase} onClick={this.removeSynonym.bind(this, row.id, syn)}>
                                      {syn}
                                    </a>
                                  </div>
                                )
                              })}
                            </TableCell>
                            <TableCell className={classes.addSynColumn} align="right">
                              <form
                                className={classes.synSelect}
                                style={{display: synonymsDisplayed ? 'block' : 'none'}}
                                onSubmit={this.addSynonym.bind(this, row.topic)}
                              >
                                <Input
                                  name='userSynonym'
                                  type='text'
                                  className={classes.synonymInput}
                                  onChange={this.handleChange.bind(this, row.id)}>
                                </Input>
                                <Button size="small" className={classes.toggleSynonym} value='Add' type='submit'>Add</Button>
                              </form>
                            </TableCell>
                          </TableRow>
                        );
                      }
                    })}
                  </TableBody>
                </Table>
              }
              {mod > 1 && synonymsTable === false &&
                <Table className={classes.table} padding='dense'>
                  <TableHead>
                    <TableRow>
                      <TableCell>
                        Key Topic
                      </TableCell>
                      {fullTableDisplayed && contentDisplayed &&
                        <TableCell align="center"># in Content</TableCell>
                      }
                      {fullTableDisplayed && assessmentDisplayed &&
                        <TableCell align="center"># in Assessment</TableCell>
                      }
                      {fullTableDisplayed && regulationDisplayed &&
                        <TableCell align="center"># in Regulations</TableCell>
                      }
                      {recommendationDisplayed &&
                        <TableCell align="left">
                          To Improve Coverage
                          <Tooltip
                            TransitionComponent={Fade}
                            TransitionProps={{ timeout: 500 }}
                            title="Green: Small/no gap Yellow: Moderate gap Red: Significant gap"
                            classes={{ tooltip: classes.labelTooltip }}
                          >
                            <span className={classes.info2}>i</span>
                          </Tooltip>
                        </TableCell>
                      }
                      {recommendationDisplayed === false &&
                        <TableCell align="left">
                          Coverage
                        </TableCell>
                      }
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {rows.map(row => {
                      if (row.stopWord === false) {
                        return (
                          <TableRow key={row.id}>
                            <TableCell className={classes.rowStyle}>{row.topic.replace(/_/g, ' ')}</TableCell>
                            {fullTableDisplayed && contentDisplayed &&
                              <TableCell className={classes.rowStyle} align="center">{row.count}</TableCell>
                            }
                            {fullTableDisplayed && assessmentDisplayed &&
                              <TableCell className={classes.rowStyle} align="center">{row.module}</TableCell>
                            }
                            {fullTableDisplayed && regulationDisplayed &&
                              <TableCell className={classes.rowStyle} align="center">{row.objectives}</TableCell>
                            }
                            <TableCell className={
                              (() => {
                                if (recommendationDisplayed === true) {
                                  switch (row.toImprove) {
                                    case "Increase red":   return classes.red;
                                    case "Increase yellow":   return classes.yellow;
                                    case "Decrease red": return classes.red;
                                    case "Decrease yellow": return classes.yellow;
                                    default:  return classes.green;
                                  }
                                } else {
                                  switch (row.toImprove) {
                                    case "In spec":   return classes.green;
                                    default:  return classes.invisible;
                                  }
                                }
                              })()
                            }>
                              {row.toImprove.replace(' red', '').replace(' yellow', '')}
                            </TableCell>
                          </TableRow>
                        );
                      }
                    })}
                  </TableBody>
                </Table>
              }
            </MuiThemeProvider> 
          </Paper>
        }
        <Paper>
          <StopWordsSection
            panel={panel}
            nltkStopWords={nltkStopWords}
            rows={rows}
            customStopWords={customStopWords}
            stopSynonymRows={stopSynonymRows}
            stopWordRows={stopWordRows}
            stopWordsCount={stopWordsCount}
            restoreTopic={this.restoreTopic.bind(this)}
            restoreSynonym={this.restoreSynonym.bind(this)}
          >
          </StopWordsSection>
        </Paper>
      </div>
    )
  }
}

MappingSectionTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MappingSectionTable);
