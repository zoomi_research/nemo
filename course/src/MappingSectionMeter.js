import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Gauge from 'react-svg-gauge';

const styles = theme => ({
  meter: {
    float: 'left',
    padding: '20px',
    width: '25%',
  },
});

class MappingSectionMeter extends React.Component {
  render() {
    const { classes, panel, panels, comparison, coverage } = this.props;
    const mod = panel ? panel.slice(-1) : 0;
    let gauge;
    let gaugeValue;
    let gaugeColor;
    let inSpecCount;
    const codes = {
      "Module Content Against Regulations": "CR",
      "Module Content Against Assessment": "CA",
      "Module Assessment Against Regulations": "AR"
    }

    const code = codes[comparison];

    gauge = panels[mod-1];
    if (mod !== 0) {
      inSpecCount = coverage['ALL'][gauge['panelId']];
      gaugeValue = Number.parseInt(inSpecCount / 30 * 100);
      if (gaugeValue >= 70) {
        gaugeColor = 'green';
      } else if (gaugeValue >= 40) {
        gaugeColor = 'orange';
      } else {
        gaugeColor = 'red';
      }
    }

    return (
      <div>
      {mod > 1 &&
        <Paper className={classes.meter}>
          <Gauge value={gaugeValue} width={170} height={130} color={gaugeColor} label="Coverage" />
        </Paper>
      }
      </div>
    )
  }
}

MappingSectionMeter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MappingSectionMeter);
