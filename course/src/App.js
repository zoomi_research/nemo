import React, { Component } from 'react';
import ModuleSection from './ModuleSection';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
        </header>
        <ModuleSection></ModuleSection>
      </div>
    );
  }
}

export default App;
