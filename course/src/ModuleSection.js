import React from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MappingSection from './MappingSection';
import ObjectiveSection from './ObjectiveSection';

const styles = theme => ({
  root: {
    width: '20%',
    float: 'left',
    paddingLeft: '10px',
  },
  title: {
    fontSize: theme.typography.pxToRem(16),
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10,
  },
  mapping: {
    width: '55%',
    paddingLeft: '20px',
    float: 'left',
  },
  objective: {
    width: '20%',
    float: 'left',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '70%',
    flexShrink: 0,
  },
});

class ModuleSection extends React.Component {
  state = {
    expanded: null,
    title: null,
    panels: [],
    nltkStopWords: [],
    selectedPanel: 'panel1',
    rows: [],
    coverage: null
  };

  constructor(props) {
    super(props);
    this.updateCoverage = this.updateCoverage.bind(this);
  }

  async updateCoverage() {
    console.log('CLICKED');
    const { coverage } = this.state;
    try {
      const response = await fetch('api/content');
      const json = await response.json();
      this.setState( {
        coverage: json.coverage
      });
      console.log('SUCCESSFULLY UPDATED Coverage');
    } catch (error) {
      console.log('Error updating coverage');
    }
  }

  async componentWillMount() {
    const { title, rows, coverage } = this.state;

    try {
      const response = await fetch('api/content');
      const json = await response.json();
      this.setState( {
        title: json.title,
        panels: json.modules,
        nltkStopWords: json.nltkStopWords,
        nltkWordCount: json.nltkStopWords.length,
        coverage: json.coverage
      });
      console.log('SUCCESSFULLY fetched Module and Coverage');
    } catch (error) {
      console.log('Error fetching Module');
    }

    if (rows === []) {
      try {
        console.log('INITIALIZE mapping');
        const response = await fetch('api/mapping?panel=panel1');
        const json = await response.json();
        this.setState( {
          rows: json.mapping,
        });
      } catch (error) {
        console.log('Error fetching');
      }
      console.log('INITIALIZED ROWS', this.state.rows);
    }
  };

  handleChange = panel => (event, expanded) => {
    this.setState({
      expanded: expanded ? panel : false,
      selectedPanel: panel
    });
  };

  render() {
    const { classes } = this.props;
    const { expanded, title, panels, nltkStopWords, nltkWordCount, selectedPanel, coverage } = this.state;

    return (
      <div>
        <div className={classes.root}>
          <Typography className={classes.title}>{title}</Typography>
          {panels.map((panel) =>
            <ExpansionPanel key={panel.id} expanded={expanded === panel.panelId} onChange={this.handleChange(panel.panelId)}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading}>{panel.name}</Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <ul>
                  <Typography>
                    {panel.segments.map((segment, index) =>
                      <li key={index}>{segment.name}</li>
                    )}
                  </Typography>
                </ul>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          )}
        </div>
          {/*MappingSection and ObjectiveSection need expanded panel from ModuleSection*/}
        <div className={classes.mapping}>
          <MappingSection
            expanded={expanded}
            nltkStopWords={nltkStopWords}
            nltkWordCount={nltkWordCount}
            panels={panels}
            selectedPanel={selectedPanel}
            coverage={coverage}
            updateCoverage={this.updateCoverage}
          >
          </MappingSection>
        </div>
        <div className={classes.objective}>
          <ObjectiveSection expanded={expanded}></ObjectiveSection>
        </div>
      </div>
    );
  }
}

ModuleSection.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ModuleSection);
