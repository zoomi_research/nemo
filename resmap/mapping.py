import json
import falcon
import os
from .summarizer import Summarizer

class Mapping(object):

    def on_post(self, req, resp):
        print('CALLED ON_POST')
        body = json.loads(req.stream.read())
        print('GOT HERE')
        add_stop_words = body['addStopWords']
        add_stop_synonyms = body['addStopSynonyms']
        add_synonyms = body['addSynonyms']
        remove_synonyms = body['removeSynonyms']
        panel = body['panel']
        if body['comparison'] == 'Module Content Against Regulations':
            comparison = 'CR'
        elif body['comparison'] == 'Module Content Against Assessment':
            comparison = 'CA'
        elif body['comparison'] == 'Module Assessment Against Regulations':
            comparison = 'AR'

        print('\n' * 5)
        print('******* USER INPUTS *****')
        print('Panel: ', panel)
        print('Param comparison', body['comparison'])
        print('Comparison: ', comparison)
        print('Stop Words: ')
        marked_stop = []
        for s in add_stop_words:
            print(s['topic'], s['index'], s['id'])
            marked_stop.append(s['index'])
        for s in add_stop_synonyms:
            for syn in s['synonyms']:
                print(syn)
        print('Add Synonyms: #TODO')
        print('Remove Synonyms: #TODO')
        for s in add_synonyms:
            print(s['topic'])
        print('\n' * 5)
        user_inputs = {
            'mark_as_stopword': marked_stop,
            'add_syn': [],
            'delete_syn': [],
            'unmark_as_stopword': []
        }
        print('User inputs:', user_inputs)
        self.update_summary(user_inputs)

        json_file = 'mapping-{}.json'.format(comparison)
        file_path = os.path.join(os.getcwd(), 'resmap', 'json', json_file)
        print(json_file)

        with open(file_path, 'r') as f:
            print('OPENED file', json_file)
            doc = json.load(f)
            body = { "mapping": doc['mapping'][panel] }

            resp.body = json.dumps(body)
            resp.status = falcon.HTTP_200


    def on_get(self, req, resp):
        print('CALLED ON_GET')
        print(req.params)
        if 'panel' in req.params:
            panel = req.params['panel']
            if 'comparison' in req.params:
                param = req.params['comparison']
                print('Param comparison', param)
                if param == 'Module Content Against Regulations':
                    comparison = 'CR'
                elif param == 'Module Content Against Assessment':
                    comparison = 'CA'
                elif param == 'Module Assessment Against Regulations':
                    comparison = 'AR'
            else:
                print('No Param comparison provided, defaulting to CR')
                comparison = 'CR'

            print('******* Initial *****')

            json_file = 'mapping-{}.json'.format(comparison)
            file_path = os.path.join(os.getcwd(), 'resmap', 'json', json_file)
            with open(file_path, 'r') as f:
                print('OPENED file', json_file)
                doc = json.load(f)
                body = { "mapping": doc['mapping'][panel] }
                resp.body = json.dumps(body)
                resp.status = falcon.HTTP_200
        else:
            print('GOT HERE')
            body = "{\"message\": \"PLACEHOLDER recommendations\"}"
            resp.body = json.dumps(body)
            resp.status = falcon.HTTP_200

    def update_summary(self, user_inputs):
        new_course = 'resmap/IBM_Prevent_fraud_new_storyboard_ngram_23'
        start_assess_ind = 52
        start_reg_ind = 87

        print('UPDATING summary')
        S = Summarizer(new_course, start_assess_ind, start_reg_ind)
        S.process_usr_inputs(user_inputs)
        res = S.generate_summary(10, True, user_inputs)
        print('DONE UPDATING summary')

        return

