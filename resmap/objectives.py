import json
import falcon
import os


class Objectives(object):

    def on_get(self, req, resp):
        file_path = os.path.join(os.getcwd(), 'resmap', 'json', 'regulations.json')
        with open(file_path, 'r') as f:
            doc = json.load(f)

            resp.body = json.dumps(doc)
            resp.status = falcon.HTTP_200
