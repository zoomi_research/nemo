import random
import numpy as np
import json
from sklearn.feature_extraction.text import CountVectorizer
import pickle
import copy

# from PyDictionary import PyDictionary
# dictionary=PyDictionary()

# w = 'life'
# print(dictionary.synonym(w))
# print(type(dictionary.synonym(w)))
# exit()


class Summarizer(object):

    def __init__(self, coursename, start_assess_ind, start_reg_ind):

        # load course words
        ordered_docs = pickle.load(open('%s.pickle' %coursename, "rb"))
        vectorizer = CountVectorizer()
        tf = vectorizer.fit_transform(ordered_docs)
        self.words = vectorizer.get_feature_names()  # a list of words; a word's index is used as the id of the word
        self.word_to_index = {w:i for i, w in enumerate(self.words)} # word to index dictionary
        self.doc_word_matrix = np.asarray(tf.todense())

        stopwords = open('static/cta_stoplist.txt', 'r').readlines()
        self.stopwords = set([word.rstrip('\n').lower() for word in stopwords])
        self.indexes_of_stopwords = set([self.word_to_index.get(w, -1) for w in self.stopwords])
        self.indexes_of_stopwords.remove(-1)


        self.start_assess_ind = start_assess_ind
        self.start_reg_ind = start_reg_ind

        self.synonym_to_orignal = {}
        self.original_to_synonyms = {}

        self.module_to_slideIndexes = {0: range(0, 7),  # intro and menu
                                       1: range(7, 13), # WHAT IS THE IMPACT OF BRIBERY, CORRUPTION AND FRAUD?
                                       2: range(13, 20), # WHAT IS FRAUD?
                                       3: range(20, 27), # HOW DO I DETECT AND PREVENT FRAUD?
                                       4: range(27, 31), # HOW DO I ESCALATE AND MANAGE FRAUD?
                                       5: range(31, 39), # WHAT IS BRIBERY AND CORRUPTION?
                                       6: range(39, 47), # HOW CAN I PREVENT BRIBERY AND CORRUPTION?
                                       7: range(47, 49)} # HOW DO I DETECT AND ESCALATE BRIBERY AND CORRUPTION?


        # 0 - 43    MAXCMS-168-424_Fraud_Procedures.pdf
        # 44 - 81   MAXCMS-168-424_Fraud_Typologies_External_Fraud.pdf
        # 82 - 85   MAXCMS-211-114_FraudPolicy_Level2.pdf                       range(87 + 82,  87 + 85)
        # 86 - 100  MAXCMS-211-5383_AntiBriberyandAntiCorruptionFAQ.pdf
        # 101 - 102 MAXCMS-211-5383_Fact_Sheet_ABAC_Policy_and_Requirements.pdf
        # 103 - 125   MAXCMS-211-97_ABACRequirements_Level2 (1).pdf
        # 126 - 132   MAXCMS-211-97_Anti_Bribery_and_Anti_Corruption_Policy.pdf    range(87 + 126,  87 + 132)


        # full pages
        # FraudPolicy_Level2,  Anti_Bribery_and_Anti_Corruption_Policy = range(87 + 82, 87 + 85 + 1),  range(87 + 126, 87 + 132 + 1)

        # exclude irrelevant pages
        FraudPolicy_Level2, Anti_Bribery_and_Anti_Corruption_Policy = list(range(87 + 82, 87 + 82 + 3)), list(range(87 + 126, 87 + 132))

        self.module_to_regIndexes = {  0: [],
                                       1: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       2: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       3: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       4: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       5: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       6: Anti_Bribery_and_Anti_Corruption_Policy,
                                       7: Anti_Bribery_and_Anti_Corruption_Policy}

        self.assessment_length = sum([sum(row) for row in self.doc_word_matrix[start_assess_ind:start_reg_ind, :]])



    def add_synonym(self, original, syn):

        if syn not in self.synonym_to_orignal:
            self.synonym_to_orignal[syn] = set([original])
        else:
            self.synonym_to_orignal[syn].add(original)

        if original not in self.original_to_synonyms:
            self.original_to_synonyms[original] = set([syn])
        else:
            if syn in self.original_to_synonyms[original]:
                self.original_to_synonyms[original].add(syn)


    def delete_synonym(self, original, syn):

        if original in self.original_to_synonyms:
            if syn in self.original_to_synonyms[original]:
                self.original_to_synonyms[original].remove(syn)
            if not self.original_to_synonyms[original]:
                del self.original_to_synonyms[original] # remove empty key in dict

        if syn in self.synonym_to_orignal:
            if original in self.synonym_to_orignal[syn]:
                self.synonym_to_orignal[syn].remove(original)
            if not self.synonym_to_orignal[syn]:
                del self.synonym_to_orignal[syn]


    def add_stopword(self, list_or_words):
        for w in list_or_words:
            self.indexes_of_stopwords.add(w)

    def delete_stopword(self, list_or_words):
        for w in list_or_words:
            self.indexes_of_stopwords.remove(w)


    def process_usr_inputs(self, usr_inputs):

        # usr_inputs = {'add_syn': [['synonym string', 'original word string'], [7, 8], [9, 18]],
        #               'delete_syn': [[5,6], [7,9], [19,67]],
        #               'mark_as_stopword': [1,2,3,4,5,6,7],
        #               'unmark_as_stopword': [1,2,3,4,5,6]}

        # for add_syn:
        # item[0] is a syn of item[1]; Therefore item[0] will be counted as item[1] and in frontend.
        # item[0] is removed from display, item[1] is kept.

        # for delete_syn:
        # item[0] is a syn of item[1]; Therefore item[0] will be removed from item[1]'s syn list
        # item[0] is enabled for display, item[1] is kept.

        for item in usr_inputs['add_syn']:
            self.add_synonym(item[1], item[0])
        for item in usr_inputs['delete_syn']:
            self.delete_synonym(item[1], item[0])

        self.add_stopword(usr_inputs['mark_as_stopword'])
        self.delete_stopword(usr_inputs['unmark_as_stopword'])


    def toImproveMessage(self, val, length_ratio):

        if val < length_ratio:
            if val <= 0.2 * length_ratio:
                return 'Increase red'
            elif 0.2 * length_ratio < val <= 0.5 * length_ratio:
                return 'Increase yellow'
            else:
                return ''

        elif val > length_ratio:
            if val >= 10 * length_ratio:
                return 'Decrease red'
            elif  10 * length_ratio > val >= 2*length_ratio:
                return 'Decrease yellow'
            else:
                return ''

        else:
            return ''


    def generate_module_summary(self, m, start_assess_ind, start_reg_ind, num_words_to_show):

        # get the summary of one module, a list of tuples of the top num_words_to_show words;

        slide_indexes, reg_indexes = self.module_to_slideIndexes[m], self.module_to_regIndexes[m]
        module_length = sum([sum(row) for row in self.doc_word_matrix[slide_indexes, :]])
        reg_length = sum([sum(row) for row in self.doc_word_matrix[reg_indexes, :]])

        result = {word:{'index':word_index, 'content':0, 'assessment':0, 'regulation':0}
                  for word_index, word in enumerate(self.words)
                  if word_index not in self.synonym_to_orignal and
                  word_index not in self.indexes_of_stopwords}

        for i, word in enumerate(self.words): # need to iterate thru all words

            # skip if this word is stopword
            if i in self.indexes_of_stopwords:
                continue

            # get original words if this word is a synonym of some other word
            if i in self.synonym_to_orignal:
                words_indexes = self.synonym_to_orignal[i]
            else:
                words_indexes = [i]
            for ind in words_indexes:
                word = self.words[ind]
                result[word]['content'] += sum(self.doc_word_matrix[slide_indexes, ind])
                result[word]['assessment'] += sum(self.doc_word_matrix[start_assess_ind:start_reg_ind, ind])
                result[word]['regulation'] += sum(self.doc_word_matrix[reg_indexes, ind])
                result[word]['slideIndexes'] = [ x for x in slide_indexes if self.doc_word_matrix[x, ind] >= 1 ]






        length_ratio_assessment_vs_regulation = (self.assessment_length + 1) / float(reg_length + 1)
        length_ratio_content_vs_assessment = (module_length + 1) / float(self.assessment_length + 1)
        length_ratio_content_vs_regulation = (module_length + 1) / float(reg_length + 1)

        # print(length_ratio_content_vs_regulation)

        for w in result:
            count_ratio = (result[w]['assessment'] + 1)/float(result[w]['regulation'] + 1)
            result[w]['toImproveAssessmentVsRegulation'] = self.toImproveMessage(count_ratio,
                                                                                      length_ratio_assessment_vs_regulation)


            count_ratio = (result[w]['content']+1)/float(result[w]['assessment'] + 1)
            result[w]['toImproveContentVsAssessment'] = self.toImproveMessage(count_ratio, length_ratio_content_vs_assessment)


            count_ratio = (result[w]['content']+1) / float(result[w]['regulation'] + 1)
            result[w]['toImproveContentVsRegulation'] = self.toImproveMessage(count_ratio, length_ratio_content_vs_regulation)


        def sort_key_by_contentAssessment(pair):
            k, v = pair
            keys = v['content'] + v['assessment']
            return keys

        def sort_key_by_assessment(pair):
            k, v = pair
            keys = v['assessment']
            return keys

        def sort_key_by_contentReg(pair):
            k, v = pair
            keys = v['content'] + v['regulation']
            return keys


        def sort_key_by_content(pair):
            k, v = pair
            keys = v['content']
            return keys


        output = {}
        for comparison in ['ContentVsRegulation', 'ContentVsAssessment', 'AssessmentVsRegulation']:
            if comparison == 'ContentVsRegulation':
                result_sorted = sorted(result.items(), key=sort_key_by_content, reverse=True)
            elif comparison == 'ContentVsAssessment':
                result_sorted = sorted(result.items(), key=sort_key_by_contentAssessment, reverse=True)
            else:
                result_sorted = sorted(result.items(), key=sort_key_by_assessment, reverse=True)

            if num_words_to_show <= len(result_sorted):
                result_sorted = result_sorted[:num_words_to_show]

            temp_list = copy.deepcopy(result_sorted)

            if comparison == 'ContentVsRegulation':
                for x in temp_list:
                    del x[1]['toImproveAssessmentVsRegulation']
                    del x[1]['toImproveContentVsAssessment']
                    x[1]['toImprove'] = x[1].pop('toImproveContentVsRegulation')

            elif comparison == 'ContentVsAssessment':
                for x in temp_list:
                    del x[1]['toImproveAssessmentVsRegulation']
                    del x[1]['toImproveContentVsRegulation']
                    x[1]['toImprove'] = x[1].pop('toImproveContentVsAssessment')
            else:
                for x in temp_list:
                    del x[1]['toImproveContentVsAssessment']
                    del x[1]['toImproveContentVsRegulation']
                    x[1]['toImprove'] = x[1].pop('toImproveAssessmentVsRegulation')

            output[comparison] = temp_list


        return output


    def generate_summary(self, num_words_to_show):

        res = {}
        for m in self.module_to_slideIndexes:

            # if m!= 4:
            #     continue

            res[m] = self.generate_module_summary(m, start_assess_ind, start_reg_ind, num_words_to_show)

            for key in res[m]:
                print(m, key)
                for item in res[m][key]:

                    print(item)
                print('\n ------------------ ')

        return res


#############################################################################################################################
#############################################################################################################################
#############################################################################################################################



if __name__ == '__main__':


    # 'IBM_Prevent_fraud_new_storyboard_ngram_1'
    new_course, start_assess_ind, start_reg_ind = 'IBM_Prevent_fraud_new_storyboard_ngram_23', 52, 87

    # new_course = 'IBM_Prevent_fraud_new_storyboard_ngram_1'

    S = Summarizer(new_course, start_assess_ind, start_reg_ind)
    res = S.generate_summary(10)
    import pdb
    pdb.set_trace()
    exit()

    usr_inputs = {'add_syn': [[3,5], [7, 8], [9, 18]],
                  'delete_syn': [[5,6], [7,9], [19,67]],
                  'mark_as_stopword': [1,2,3,4,5,6,7],
                  'unmark_as_stopword': [1,2,3,4,5,6]}
    S.process_usr_inputs(usr_inputs)
    res = S.generate_summary(10)
