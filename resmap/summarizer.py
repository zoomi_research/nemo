import random
import numpy as np
import json
from sklearn.feature_extraction.text import CountVectorizer
import pickle

# from PyDictionary import PyDictionary
# dictionary=PyDictionary()


class Summarizer(object):


    def __init__(self, coursename, start_assess_ind, start_reg_ind, called_from_main=False):
        if called_from_main:
            stop_file='static/cta_stoplist.txt'
        else:
            stop_file='resmap/static/cta_stoplist.txt'
        self.called_from_main = called_from_main

        # load course words
        ordered_docs = pickle.load(open('%s.pickle' %coursename, "rb"))
        vectorizer = CountVectorizer()
        tf = vectorizer.fit_transform(ordered_docs)
        self.words = vectorizer.get_feature_names()  # a list of words; a word's index is used as the id of the word
        self.word_to_index = {w:i for i, w in enumerate(self.words)} # word to index dictionary
        self.doc_word_matrix = np.asarray(tf.todense())

        stopwords = open(stop_file, 'r').readlines()
        self.stopwords = set([word.rstrip('\n').lower() for word in stopwords])
        self.indexes_of_stopwords = set([self.word_to_index.get(w, -1) for w in self.stopwords])
        self.indexes_of_stopwords.remove(-1)


        self.start_assess_ind = start_assess_ind
        self.start_reg_ind = start_reg_ind

        self.synonym_to_orignal = {}
        self.original_to_synonyms = {}

        self.module_to_slideIndexes = {0: range(0, 7),  # intro and menu
                                       1: range(7, 13), # WHAT IS THE IMPACT OF BRIBERY, CORRUPTION AND FRAUD?
                                       2: range(13, 20), # WHAT IS FRAUD?
                                       3: range(20, 27), # HOW DO I DETECT AND PREVENT FRAUD?
                                       4: range(27, 31), # HOW DO I ESCALATE AND MANAGE FRAUD?
                                       5: range(31, 39), # WHAT IS BRIBERY AND CORRUPTION?
                                       6: range(39, 47), # HOW CAN I PREVENT BRIBERY AND CORRUPTION?
                                       7: range(47, 49)} # HOW DO I DETECT AND ESCALATE BRIBERY AND CORRUPTION?

        FraudPolicy_Level2, Anti_Bribery_and_Anti_Corruption_Policy = list(range(87 + 82, 87 + 82 + 3)), list(range(87 + 126, 87 + 132))

        self.module_to_regIndexes = {  0: [],
                                       1: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       2: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       3: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       4: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       5: FraudPolicy_Level2 + Anti_Bribery_and_Anti_Corruption_Policy,
                                       6: Anti_Bribery_and_Anti_Corruption_Policy,
                                       7: Anti_Bribery_and_Anti_Corruption_Policy}


        self.assessment_length = sum([sum(row) for row in self.doc_word_matrix[start_assess_ind:start_reg_ind, :]])
        # self.reg_length = sum([sum(row) for row in self.doc_word_matrix[start_reg_ind:, :]])



    def add_synonym(self, original, syn):

        if syn not in self.synonym_to_orignal:
            self.synonym_to_orignal[syn] = set([original])
        else:
            self.synonym_to_orignal[syn].add(original)

        if original not in self.original_to_synonyms:
            self.original_to_synonyms[original] = set([syn])
        else:
            if syn in self.original_to_synonyms[original]:
                self.original_to_synonyms[original].add(syn)


    def delete_synonym(self, original, syn):

        if original in self.original_to_synonyms:
            if syn in self.original_to_synonyms[original]:
                self.original_to_synonyms[original].remove(syn)
            if not self.original_to_synonyms[original]:
                del self.original_to_synonyms[original] # remove empty key in dict

        if syn in self.synonym_to_orignal:
            if original in self.synonym_to_orignal[syn]:
                self.synonym_to_orignal[syn].remove(original)
            if not self.synonym_to_orignal[syn]:
                del self.synonym_to_orignal[syn]


    def add_stopword(self, list_or_words):
        for w in list_or_words:
            self.indexes_of_stopwords.add(w)

    def delete_stopword(self, list_or_words):
        for w in list_or_words:
            self.indexes_of_stopwords.remove(w)


    def process_usr_inputs(self, usr_inputs):

        # usr_inputs = {'add_syn': [[3,5], [7, 8], [9, 18]],
        #               'delete_syn': [[5,6], [7,9], [19,67]],
        #               'mark_as_stopword': [1,2,3,4,5,6,7],
        #               'unmark_as_stopword': [1,2,3,4,5,6]}
        #
        # for add_syn:
        # item[0] is a syn of item[1]; Therefore item[0] will be counted as item[1] and in frontend.
        # item[0] is removed from display, item[1] is kept.

        # for delete_syn:
        # item[0] is a syn of item[1]; Therefore item[0] will be removed from item[1]'s syn list
        # item[0] is enabled for display, item[1] is kept.

        for item in usr_inputs['add_syn']:
            self.add_synonym(item[1], item[0])
        for item in usr_inputs['delete_syn']:
            self.delete_synonym(item[1], item[0])

        self.add_stopword(usr_inputs['mark_as_stopword'])
        self.delete_stopword(usr_inputs['unmark_as_stopword'])


    def generate_module_summary(self, m, num_words_to_show):

        # get the summary of one module, a list of tuples of the top num_words_to_show words;
        # example output:
        # result = [(u'fraud', {'content': 57, 'index': 1243, 'regulation': 738, 'assessment': 10}),
                    # (u'anz', {'content': 15, 'index': 164, 'regulation': 388, 'assessment': 28}),
                    # (u'risk', {'content': 8, 'index': 2625, 'regulation': 356, 'assessment': 9}),
                    # (u'policy', {'content': 11, 'index': 2234, 'regulation': 351, 'assessment': 6}),
                    # (u'party', {'content': 0, 'index': 2155, 'regulation': 305, 'assessment': 8}),
                    # (u'customer', {'content': 6, 'index': 731, 'regulation': 277, 'assessment': 14}),
                    # (u'business', {'content': 7, 'index': 407, 'regulation': 257, 'assessment': 12}),
                    # (u'financial', {'content': 1, 'index': 1199, 'regulation': 269, 'assessment': 5}),
                    # (u'corruption', {'content': 34, 'index': 689, 'regulation': 202, 'assessment': 26}),
                    # (u'bribery', {'content': 24, 'index': 389, 'regulation': 214, 'assessment': 24})]


        slide_indexes, reg_indexes = self.module_to_slideIndexes[m], self.module_to_regIndexes[m]
        module_length = sum([sum(row) for row in self.doc_word_matrix[slide_indexes, :]])
        reg_length = sum([sum(row) for row in self.doc_word_matrix[reg_indexes, :]])

        result = {word:{'index':word_index, 'content':0, 'assessment':0, 'regulation':0}
                  for word_index, word in enumerate(self.words)
                  if word_index not in self.synonym_to_orignal and
                  word_index not in self.indexes_of_stopwords}

        for i, word in enumerate(self.words): # need to iterate thru all words

            # skip if this word is stopword
            if i in self.indexes_of_stopwords:
                continue

            # get original words if this word is a synonym of some other word
            if i in self.synonym_to_orignal:
                words_indexes = self.synonym_to_orignal[i]
            else:
                words_indexes = [i]
            for ind in words_indexes:
                word = self.words[ind]
                result[word]['content'] += sum(self.doc_word_matrix[slide_indexes, ind])
                result[word]['assessment'] += sum(self.doc_word_matrix[self.start_assess_ind:self.start_reg_ind, ind])
                result[word]['regulation'] += sum(self.doc_word_matrix[reg_indexes, ind])

                result[word]['slideIndexes'] = [ x for x in slide_indexes if self.doc_word_matrix[x, ind] >= 1 ]


        def toImproveMessage(count_ratio, length_ratio):
            if count_ratio < length_ratio:
                if count_ratio <= 0.2 * length_ratio:
                    return 'Increase red'
                elif 0.2 * length_ratio < count_ratio <= 0.5 * length_ratio:
                    return 'Increase yellow'
                else:
                    return 'In spec'

            elif count_ratio > length_ratio:
                if count_ratio >= 10 * length_ratio:  # 2 vs 1/2 ;  5 vs 1/5
                    return 'Decrease red'
                elif  10 * length_ratio > count_ratio >= 2*length_ratio:  # 2 vs 1/2 ;  5 vs 1/5
                    return 'Decrease yellow'
                else:
                    return 'In spec'




        length_ratio_assessment_vs_regulation = (self.assessment_length + 1) / float(reg_length + 1)
        length_ratio_content_vs_assessment = (module_length + 1) / float(self.assessment_length + 1)
        length_ratio_content_vs_regulation = (module_length + 1) / float(reg_length + 1)

        for w in result:
            count_ratio = (result[w]['assessment'] + 1)/float(result[w]['regulation'] + 1)
            result[w]['toImproveAssessmentVsRegulation'] = toImproveMessage(count_ratio,
                                                                                      length_ratio_assessment_vs_regulation)

            count_ratio = (result[w]['content'] + 1)/float(result[w]['assessment'] + 1)
            result[w]['toImproveContentVsAssessment'] = toImproveMessage(count_ratio, length_ratio_content_vs_assessment)

            count_ratio = (result[w]['content'] + 1) / float(result[w]['regulation'] + 1)
            result[w]['toImproveContentVsRegulation'] = toImproveMessage(count_ratio, length_ratio_content_vs_regulation)

        def sort_key_by_assessment(pair):
            k, v = pair
            keys = v['assessment']
            return keys

        def sort_key_by_contentReg(pair):
            k, v = pair
            keys = v['content'] + v['assessment']
            return keys

        def sort_key_by_content(pair):
            k, v = pair
            keys = v['content']
            return keys


        output = {}
        temp_list = sorted(result.items(), key=sort_key_by_content, reverse=True)
        if num_words_to_show <= len(temp_list):
            temp_list = temp_list[:num_words_to_show]
        output['ContentVsRegulation'] = temp_list

        temp_list = sorted(result.items(), key=sort_key_by_assessment, reverse=True)
        if num_words_to_show <= len(temp_list):
            temp_list = temp_list[:num_words_to_show]

        output['ContentVsAssessment'] = temp_list
        output['AssessmentVsRegulation'] = temp_list

        return output


    def save_mapping_to_file(self, code, res, update, user_inputs):
        mapper = {'mapping': {'panel1': [], 'panel2': [], 'panel3': [], 'panel4': [], 'panel5': [], 'panel6': [], 'panel7': [], 'panel8': []}, 'userInputs': user_inputs, 'update': update }

        print('Assigning toImprove values')
        for i in range(len(res)):
            module = res[i]
            panel = 'panel{}'.format(i+1)
            for j in range(len(module)):
                topic, attrs = module[j]

                if code == 'CA':
                    toImprove = attrs['toImproveContentVsAssessment']
                elif code == 'CR':
                    toImprove = attrs['toImproveContentVsRegulation']
                elif code == 'AR':
                    toImprove = attrs['toImproveAssessmentVsRegulation']

                synonyms = []
                obj = {
                    'id': j + 1,
                    'index': int(attrs['index']),
                    'topic': topic,
                    'count': int(attrs['content']),
                    'module': int(attrs['assessment']),
                    'objectives': int(attrs['regulation']),
                    'toImprove': toImprove,
                    'stopWord': False,
                    'synonyms': synonyms,
                }
                mapper['mapping'][panel].append(obj)

        if self.called_from_main:
            json_file = 'json/mapping-{}.json'.format(code)
        else:
            json_file = 'resmap/json/mapping-{}.json'.format(code)

        with open(json_file, 'w') as f:
            print('Saving JSON file', json_file)
            json.dump(mapper, f, indent=2)

        return 'Done saving files'


    def generate_summary(self, num_words_to_show, update=False, user_inputs={}):

        res = {}
        for m in self.module_to_slideIndexes:
            res[m] = self.generate_module_summary(m, num_words_to_show)


        res_CA = []
        res_CR = []
        res_AR = []

        for value in res.values():
            res_CA.append(value['ContentVsAssessment'])
            res_CR.append(value['ContentVsRegulation'])
            res_AR.append(value['AssessmentVsRegulation'])

        print('READY to save mapping with user inputs', user_inputs)
        self.save_mapping_to_file('CA', res_CA, update, user_inputs)
        self.save_mapping_to_file('CR', res_CR, update, user_inputs)
        self.save_mapping_to_file('AR', res_AR, update, user_inputs)

        coverage = {
          "CR": {},
          "CA": {},
          "AR": {},
          "ALL": {}
        }


        for num in range(1,9):
            panel = "panel{}".format(num)
            coverage["ALL"][panel] = 0

        for code in ['CR', 'CA', 'AR']:
            if self.called_from_main:
                json_file = 'json/mapping-{}.json'.format(code)
            else:
                json_file = 'resmap/json/mapping-{}.json'.format(code)
            percent_coverage = {}

            with open(json_file) as f:
                data = json.load(f)
                for panel, values in data['mapping'].items():
                    in_spec = len([value for value in values if value['toImprove'] == 'In spec'])
                    percent_coverage[panel] = in_spec / 10 * 100
                    coverage["ALL"][panel] += in_spec
                import pdb
                pdb.set_trace()

            for panel_key in list(data['mapping'].keys()):
                coverage[code][panel_key] = percent_coverage[panel_key]

        if self.called_from_main:
            source_file = 'json/orig_modules.json'
            coverage_file = 'json/modules.json'
        else:
            source_file = 'resmap/json/orig_modules.json'
            coverage_file = 'resmap/json/modules.json'
        with open(source_file, 'r') as f:
            modules = json.load(f)
            modules["coverage"] = coverage

        with open(coverage_file, 'w') as f:
            json.dump(modules, f, indent=2)
            print('Saving final module file', coverage_file)


        return res


#############################################################################################################################
#############################################################################################################################
#############################################################################################################################



if __name__ == '__main__':

    # w = 'secure'
    # print(dictionary.synonym(w))
    # print(type(dictionary.synonym(w)))
    # exit()

    'IBM_Prevent_fraud_new_storyboard_ngram_1'
    new_course, start_assess_ind, start_reg_ind = 'IBM_Prevent_fraud_new_storyboard_ngram_23', 52, 87

    # new_course = 'IBM_Prevent_fraud_new_storyboard_ngram_1'

    called_from_main = True
    S = Summarizer(new_course, start_assess_ind, start_reg_ind, called_from_main)
    res = S.generate_summary(10)


    # usr_inputs = {'add_syn': [[3,5], [7, 8], [9, 18]],
    #                   'delete_syn': [[5,6], [7,9], [19,67]],
    #                   'mark_as_stopword': [1,2,3,4,5,6,7],
    #                   'unmark_as_stopword': [1,2,3,4,5,6]}
    # S.process_usr_inputs(usr_inputs)
