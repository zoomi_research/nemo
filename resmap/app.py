import falcon

from .content import Content
from .objectives import Objectives
from .mapping import Mapping
from .coverage import Coverage
from .images import Resource

api = application = falcon.API()

images = Resource(storage_path='.')
content = Content()
objectives = Objectives()
mapping = Mapping()
coverage = Coverage()

api.add_route('/api/objectives', objectives)
api.add_route('/api/mapping', mapping)
api.add_route('/api/images', images)
api.add_route('/api/content', content)
api.add_route('/api/coverage', coverage)
